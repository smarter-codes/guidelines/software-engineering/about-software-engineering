When writing a computer program, there might be 10 different ways to reach a result. 5 good ways, and 5 bad ways. Because [everyone wants to code fast](https://youtu.be/zHiWqnTWsn4?t=954), we might be tempted to pick a way that "appears" fast - but is also bad for our software. Its bad because as time passes the engineers become slower and introduce more bugs into software. 

At SmarterCodes we are putting together [guidelines](https://gitlab.com/smarter-codes/guidelines/about-guidelines) for [everything](https://gitlab.com/smarter-codes/guidelines). Guidelines that tell good ways and bad ways to do something. [Here](https://gitlab.com/smarter-codes/guidelines/software-engineering) we have put together guidelines for software engineering we do at SmarterCodes. 

Since Feb 2021 some 30+ engineers in SmarterCodes have contributed 150+ guidelines. 
We have put together a "table of contents" for these guidelines in [Discipline of Software Engineering at Smarter.Codes](#discipline-of-software-engineering-at-smartercodes). 
But first lets compare the options - of following guidelines or not!

## Option1: Heading directly into coding.
And get the new feature/fix working in no time (and most likely not get it right in the first go)
```mermaid
flowchart LR;
    Customer(Customer Interviews) --> |ideate| Requirements;
    Requirements --> |do| Coding;
    Coding --> |and finally|Deploy ;
    Deploy -->|fix defect| Coding;
```


## Option2: Planning > Building > Reviewing > Repeat
* First Architect your software. and have others Review it.
* Then Code of your software. Again, have others Review it.
* Then Deploy your software,. Again, have others review it.

This increase the chances of getting it right in the first go.

```mermaid
flowchart LR;
    Customer(Customer Interviews) --> |ideate| Product_Design;
    Product_Design(Product Design) --> |turn into| Requirements;
    Requirements --> |plan| Architecture;
    Architecture --> |review. fix defect| Requirements;
    Architecture --> |do| Coding;
    Coding --> |review. fix defect| Architecture;
    Coding --> |and finally| Deploy;
    Deploy --> |review. fix defect| Coding;
    Deploy --> |but constantly| Monitor;
    Monitor --> |review. fix defect| Deploy;
```
Its surprising to several experienced engineers, that Option1 and Option2 both take the same time!
If you haven't practiced Option2 in a few production projects - Yes it does takes you  extra time (some 15-20days doing first step to last in Option2, versus just 2-3 days in Option1).
But after 1-2 production projects you become habitual and are able to finish first step to last step in same 2-3days.

Please give a patience of 1-2 months to get familier with option2.
As per [this study](https://smartbear.com/blog/the-secret-to-code-quality/), 26% of software teams reported that Option2 reduced the project time and costs.


# Discipline of Software Engineering at Smarter.Codes

## [Requirements](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements)
1. [Understand WHYs of the requirement](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/understand-product-design)
2. [Document Requirements](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/documenting-requirements/)
3. Review Requirements
    - Raise alarm for incomplete requirements
    - Raise alarm for ineffective requirements

## [Architecture and Design](https://gitlab.com/smarter-codes/guidelines/software-engineering/architecture-design)
4. [Turn Requirements into Architecture](https://gitlab.com/smarter-codes/guidelines/software-engineering/architecting)
5. Review Architecture
    - Raise alarm for incomplete architecture
    - Raise alarm for inefficient architecture

## [Coding](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding)
6. [Turn Architecture into Code](https://gitlab.com/smarter-codes/guidelines/software-engineering/programming-foundations)
7. [Review code](https://gitlab.com/smarter-codes/guidelines/software-engineering/code-review)
    - Raise alarm for incomplete code
    - Raise alarm for inefficient code

## [Deploy](https://gitlab.com/smarter-codes/guidelines/software-engineering/deploy)
8. Deploy code in `dev` server
9. Have an external team test our work. Later deploy on `production` server

## [Monitor](https://gitlab.com/smarter-codes/guidelines/software-engineering/monitor)
10. [Monitor performance](https://gitlab.com/smarter-codes/guidelines/software-engineering/application-performance-monitoring) of new code on `dev` and `production` server
11. Reopen step from above based on performance observed in last step

# Contributing
The discipline listed above acts as a 'Table of Contents' of all the projects you see under [Software Engineering Guidelines](https://gitlab.com/smarter-codes/guidelines/software-engineering). We would like you to contribute by 
* Expanding the Table of Contents above
* Linking it with the appropriate guideline projects
* Contribute inside each Guideline project, improve their markdown, their issues
